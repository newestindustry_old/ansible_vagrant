#!/bin/sh

docker build -t crosbymichael/dockerui github.com/crosbymichael/dockerui
docker run -d -p 9000:9000 -v /var/run/docker.sock:/docker.sock crosbymichael/dockerui -e /docker.sock

docker tag newestindustry/api mach002.t.newestindustry.nl:5000/api
docker push mach002.t.newestindustry.nl:5000/api